---
title: "Podcast"
weight: 0
---

- [EmacsTalk](https://emacs.liujiacai.net/)
- [RustTalk](https://rusttalk.github.io/)
- [ASDF](https://liujiacai.net/asdf)
